# gfinity

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## One thing to point out
When going to matchmaking lobby, the timer starts counting from '1:42' and it will update the website content at '1:30' to 'simulate' finding a match for player and page will update.

## List of components in project

### `Badges`

**Description:** Two badges by default one with border and one with black background
#### `default`
**Props:**
- text | required | type: String |
- bgColor | default: bg-black | type: String |
- txtColor | default: text-white | type: String |
#### `Border`
**Props:**
- text | required |
- borderColor | default: border-black | type: String |
- thickness | default:border | type: String |

### `Borderbottom`
**Description:** Simple border bottom div

**Props:**
- borderColor | default: border-lightGrey | type: String |
- marginTop | type: String |

### `Box`
**Description:** Box with appropriate padding for things like logo or go back button
**Props:**
- bgColor | default: bg-black | type: String |
- textColor | default: text-white | type: String |

### `Card`
**Description:** Card for players displaying their information and image

**Props:**
- player | required | type: Boolean |
- name | type: String |
- role | type: String |
- Image | type: String |
- ready | type:  Boolean |

### `Header`
**Description:** Displaying logo and nabber button

**Props:**
- showBg | default: false | type: Boolean |

### `Navbar`
**Description:** Main menu container with items

#### `default`
**Props:**
- items | required | type: Array |
#### `item`
**Props:**
- name | required | type: String |
- external | required | type: Boolean |
- link | required | type: String |

### `Nextto`
**Description:** Put two elements next to each other

**Props:**
- spacing | type: String |
- padding | type: String |
- leftAlign | type: String |
- rightAlign | type: String |
- justify | type: String |

### `Options`
**Description:** Generating list of options in dropdown menu

**Props:**
- options | required | type: Array |

### `Players`
**Description:** Displaying grid of players and invite player card

**Props:**
- players | required | type: Array |
