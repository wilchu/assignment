const colors = require('tailwindcss/colors');
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    letterSpacing: {
      wide: '.025em',
      widest: '.23em',
    },
    colors: {
      primaryBg: '#DFE0D9',
      darkBlue: '#293894',
      lightGrey: '#CFD1C7',
      darkGrey: '#6B6B65',
      red: '#E94235',
      black: colors.black,
      white: colors.white,
    }
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
}
